package me.nitram.custommobsplugin.utils;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;


public class UtilityChance
{

    public static int Random (ArrayList<Integer> chances)
    {
        AtomicInteger sum = new AtomicInteger();
        chances.forEach(sum::addAndGet);

        int random = ThreadLocalRandom.current().nextInt(1, sum.get() + 1);
        for (int i = 0; i < chances.size(); i++) {
            if (random <= 0) {
                return --i;
            }
            random -= chances.get(i);
        }

        return chances.size() - 1;
    }
}
