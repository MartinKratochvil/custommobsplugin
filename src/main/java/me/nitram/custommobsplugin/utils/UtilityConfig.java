package me.nitram.custommobsplugin.utils;

import com.google.common.base.Charsets;
import me.nitram.custommobsplugin.CustomMobsPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class UtilityConfig {

    // Název configu bez ".yml"
    private String fileName;

    // Proměnná pro samotný soubor
    private File file;

    // Proměnná pro data spojená se souborem
    // !!! KONFIGURACE != SOUBOR !!!
    private FileConfiguration configuration;

    public UtilityConfig(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Setupne config (vytvoří file, pokud je potřeba, loadne z něj data, atd.)
     *
     * @param downloadFromResources Pokud soubor neexistuje, má se vložit soubor z "resources" složky (true), nebo jen vytvořit prázdný soubor? (false)
     */
    public void setup(boolean downloadFromResources) {
        // Zkontrolujeme, jestli existuje složka "/plugins/NázevPluginu"
        if (!CustomMobsPlugin.getInstance().getDataFolder().exists()) {
            // Pokud neexistuje, tak ji tímto vytvoříme
            CustomMobsPlugin.getInstance().getDataFolder().mkdir();
        }

        // Pokusíme se loadnout soubor "nazev_souboru.yml"
        file = new File(CustomMobsPlugin.getInstance().getDataFolder() + "/"+fileName+".yml");

        // Zkontrolujeme, jestli existuje
        if (!file.exists()) {

            // Pokud neexistuje, tak jej buď stáhneme z resources, nebo jen vytvoříme prázdný soubor!
            if (downloadFromResources) {
                // Stáhnutí z "resources"
                CustomMobsPlugin.getInstance().saveResource(fileName+".yml", false);
            } else {
                try {
                    // Vytvoření prázdného souboru
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        // Nakonec bacha! Konfigurace a samotný soubor NENÍ TOTÉŽ!
        // Konfigurace = Data ze souboru
        // Soubor = Noo... prostě soubor
        configuration = YamlConfiguration.loadConfiguration(file);

        // Tohle moc rozebírat nebudu, zatím prostě berme, že to je důležitý
        reloadConfig();
    }

    private void reloadConfig() {
        configuration = YamlConfiguration.loadConfiguration(file);
        InputStream defConfigStream = CustomMobsPlugin.getInstance().getResource(fileName+".yml");
        if (defConfigStream != null) {
            configuration.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, Charsets.UTF_8)));
        }
    }

    /**
     * Vrací KONFIGURACI (tedy data ze souboru). Můžete je upravovat, ale bacha!
     * To, že je upravíte v KONFIGURACI neznamená, že je upravíte i v SOUBORU!
     * Data pak musíte ULOŽIT pomocí metody #save() níže!
     *
     * @return
     */
    public FileConfiguration get() {
        return configuration;
    }

    public void save() {
        try {
            configuration.save(file);
        } catch (IOException ignored) {
        }
    }
}
