package me.nitram.custommobsplugin;

import me.nitram.custommobsplugin.commands.CustomMobsCommand;
import me.nitram.custommobsplugin.handlers.PlayerKillLimitHandler;
import me.nitram.custommobsplugin.listeners.EntityDeathListener;
import me.nitram.custommobsplugin.listeners.JoinQuitListener;
import me.nitram.custommobsplugin.listeners.MobSpawnListener;
import me.nitram.custommobsplugin.mobsdata.MobsData;
import me.nitram.custommobsplugin.utils.UtilityConfig;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.Objects;


public class CustomMobsPlugin extends JavaPlugin
{

    private static CustomMobsPlugin instance;

    private UtilityConfig mainConfig;

    private PlayerKillLimitHandler playerLimitHandler;


    public static CustomMobsPlugin getInstance() {
        return instance;
    }


    public UtilityConfig getMainConfig() {
        return mainConfig;
    }


    public PlayerKillLimitHandler getPlayerLimitHandler() {
        return playerLimitHandler;
    }


    @Override
    public void onEnable()
    {
        instance = this;

        mainConfig = new UtilityConfig("config");
        mainConfig.setup(true);
        MobsData.load();
        MobsData.save();

        playerLimitHandler = new PlayerKillLimitHandler();

        this.getServer().getPluginManager().registerEvents(new MobSpawnListener(), this);
        this.getServer().getPluginManager().registerEvents(new EntityDeathListener(), this);
        this.getServer().getPluginManager().registerEvents(new JoinQuitListener(), this);

        Objects.requireNonNull(this.getCommand("custommobs")).setExecutor(new CustomMobsCommand());
    }
}
