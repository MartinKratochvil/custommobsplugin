package me.nitram.custommobsplugin.handlers;

import java.util.HashMap;


public class PlayerKillLimitHandler {

    private HashMap<String, Integer> playerKillLimitMap;


    public PlayerKillLimitHandler() {
         clear();
    }


    public boolean isInLimit(String playerName) {
        if ( ! playerKillLimitMap.containsKey(playerName)) {
            return true;
        }

        return playerKillLimitMap.get(playerName) <= 10;
    }


    public void killCustomMob(String playerName) {
        if ( ! playerKillLimitMap.containsKey(playerName)) {
            playerKillLimitMap.put(playerName, 1);

            return;
        }

        playerKillLimitMap.replace(playerName, playerKillLimitMap.get(playerName) + 1);
    }


    public void clear() {
        playerKillLimitMap = new HashMap<>();
    }
}
