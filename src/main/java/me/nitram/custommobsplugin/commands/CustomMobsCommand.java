package me.nitram.custommobsplugin.commands;

import me.nitram.custommobsplugin.CustomMobsPlugin;
import me.nitram.custommobsplugin.mobsdata.MobsData;
import me.nitram.custommobsplugin.utils.UtilityParse;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class CustomMobsCommand implements CommandExecutor
{

    private static final String addMessage = ChatColor.GREEN + "Custom mob successfully created";

    private static final String addHelpMessage = ChatColor.GRAY + "/custommobs add [MobType] [CustomName] ?[Health 0-1024] ?[SpawnChance >0]";

    private static final String reloadMessage = ChatColor.GREEN + "Custom mobs successfully reloaded";

    private static final String helpMessage = ChatColor.GRAY + "/custommobs add - to add a new custom mob" + System.lineSeparator() + "/custommobs reload - to reload custom mobs" + System.lineSeparator() + "/custommobs help - to show this help menu";

    private static final String errorPermission = ChatColor.RED + "You don't have permission for that command";

    private static final String errorMessageFewArgs = ChatColor.RED + "Error too few arguments";

    private static final String errorMessageWrongArg = ChatColor.RED + "Error wrong argument";

    private static final String errorMessageEntityType = ChatColor.RED + "Error given mob doesn't exist. As separator use underscore '_'. Example \"cave_spider\"";

    private static final String errorMessageHealth = ChatColor.RED + "Error health must be decimal number from 0 to 1024 including";

    private static final String errorMessageSpawnChance = ChatColor.RED + "Error spawn chance must be integer number greater than 0";

    private CommandSender commandSender;

    private String[] args;


    @Override
    @SuppressWarnings("NullableProblems")
    public boolean onCommand(
        CommandSender commandSender,
        Command command,
        String s,
        String[] args
    ){
        this.commandSender = commandSender;
        this.args = args;

        if (args.length < 1) {
            commandSender.sendMessage(errorMessageFewArgs);
            commandHelp();

            return true;
        }

        switch (args[0].toLowerCase()) {
            case "add" -> commandAdd();
            case "reload" -> commandReload();
            case "help" -> commandHelp();
            default ->  {
                commandSender.sendMessage(errorMessageWrongArg);
                commandHelp();
            }
        }

        return true;
    }


    private void commandAdd()
    {
        if (commandSender instanceof Player player) {
            if ( ! player.hasPermission("custommobs.add")) {
                player.sendMessage(errorPermission);
                return;
            }
        }

        if (args.length < 3) {
            commandSender.sendMessage(errorMessageFewArgs);
            commandAddHelpMessage();

            return;
        }

        if (args.length < 4) {
            if ( ! MobsData.addCustomMob(args[1], args[2])) {
                commandSender.sendMessage(errorMessageEntityType);
                commandAddHelpMessage();

                return;
            }
            commandSender.sendMessage(addMessage);

            return;
        }

        Double health = UtilityParse.doubleTryParse(args[3]);
        if (health == null) {
            commandSender.sendMessage(errorMessageHealth);
            return;
        }

        if(health < 0 || health > 1024) {
            commandSender.sendMessage(errorMessageHealth);
            return;
        }

        if (args.length < 5) {
            if ( ! MobsData.addCustomMob(args[1], args[2], health)) {
                commandSender.sendMessage(errorMessageEntityType);
                commandAddHelpMessage();

                return;
            }
            commandSender.sendMessage(addMessage);

            return;
        }

        Integer spawnChance = UtilityParse.intTryParse(args[4]);
        if (spawnChance == null) {
            commandSender.sendMessage(errorMessageSpawnChance);
            return;
        }

        if (spawnChance <= 0) {
            commandSender.sendMessage(errorMessageSpawnChance);
            return;
        }

        if ( ! MobsData.addCustomMob(args[1], args[2], health, spawnChance)) {
            commandSender.sendMessage(errorMessageEntityType);
            commandAddHelpMessage();

            return;
        }
        commandSender.sendMessage(addMessage);
    }


    private void commandReload()
    {
        if (commandSender instanceof Player player) {
            if ( ! player.hasPermission("custommobs.reload")) {
                player.sendMessage(errorPermission);
                return;
            }
        }

        MobsData.load();
        CustomMobsPlugin.getInstance().getPlayerLimitHandler().clear();
        commandSender.sendMessage(reloadMessage);
    }


    private void commandHelp() {
        commandSender.sendMessage(ChatColor.GREEN + "Help Menu:" + System.lineSeparator() + helpMessage);
    }


    private void commandAddHelpMessage() {
        commandSender.sendMessage(addHelpMessage);
    }
}
