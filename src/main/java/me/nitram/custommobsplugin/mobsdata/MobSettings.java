package me.nitram.custommobsplugin.mobsdata;

import org.bukkit.inventory.ItemStack;
import java.util.HashMap;


public record MobSettings(
    String customName,
    double health,
    int spawnChance,
    ItemStack mainHandItem,
    float dropChance
)
{

    public HashMap<String, ?> getRawSettings() {
        return new HashMap<>() {{
            put("Name", customName);
            put("Health", health);
            put("SpawnChance", spawnChance);
            put("MainHandItem", mainHandItem);
            put("DropChance", (double)dropChance);
        }};
    }
}
