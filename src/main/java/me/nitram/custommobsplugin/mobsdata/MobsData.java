package me.nitram.custommobsplugin.mobsdata;

import me.nitram.custommobsplugin.CustomMobsPlugin;
import me.nitram.custommobsplugin.utils.UtilityConfig;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Mob;
import org.bukkit.inventory.ItemStack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


public class MobsData
{

    private static HashMap<String, MobTypes> mobsData;


    @SuppressWarnings("unchecked")
    public static void load()
    {
        mobsData = new HashMap<>();
        FileConfiguration fileConfig = CustomMobsPlugin.getInstance().getMainConfig().get();

        if ( ! fileConfig.contains("Mobs")) {
            return;
        }

        ConfigurationSection mobsSection = fileConfig.getConfigurationSection("Mobs");

        if (mobsSection == null) {
            return;
        }

        mobsSection.getKeys(false).forEach(mob -> {
            ArrayList<HashMap<String, ?>> mobRawTypes = (ArrayList<HashMap<String, ?>>) mobsSection.getList(mob);

            if (mobRawTypes == null) {
                System.out.println("Error custom mobs of entity " + mob + " are uncastable. Data structure in config is invalid, for more details check documentation");
                return;
            }
            System.out.println("Loading custom mobs of entity " + mob);

            mobsData.put(mob.toUpperCase() , new MobTypes(mobRawTypes));
        });
    }


    public static void save()
    {
        UtilityConfig mainConfig = CustomMobsPlugin.getInstance().getMainConfig();

        if ( ! mainConfig.get().contains("Mobs")) {
            mainConfig.get().createSection("Mobs");
        }

        ConfigurationSection mobsSection = mainConfig.get().getConfigurationSection("Mobs");

        getRawMobsData().forEach(Objects.requireNonNull(mobsSection)::set);
        mainConfig.save();
    }


    private static boolean addMobData(String entityType, MobSettings mobSettings)
    {
        if (mobsData.containsKey(entityType.toUpperCase())) {
            mobsData.get(entityType.toUpperCase()).getTypes().add(mobSettings);
            save();

            return true;
        }

        Class<?> entityClass;
        try {
            entityClass = EntityType.valueOf(entityType.toUpperCase()).getEntityClass();
        }
        catch (Exception ignored) {
            return false;
        }

        if (entityClass == null) {
            return false;
        }

        if ( ! Mob.class.isAssignableFrom(entityClass)) {
            return false;
        }

        mobsData.put(entityType.toUpperCase(), new MobTypes(mobSettings));
        save();

        return true;
    }


    public static boolean addCustomMob(
        String entityType,
        String customName
    ){
        return addCustomMob(
            entityType,
            customName,
            0
        );
    }


    public static boolean addCustomMob(
        String entityType,
        String customName,
        double health
    ){
        return addCustomMob(
            entityType,
            customName,
            health,
            1
        );
    }


    public static boolean addCustomMob(
        String entityType,
        String customName,
        double health,
        int spawnChance
    ){
        return addCustomMob(
            entityType,
            customName,
            health,
            spawnChance,
            new ItemStack(Material.AIR)
        );
    }


    public static boolean addCustomMob(
        String entityType,
        String customName,
        double health,
        int spawnChance,
        ItemStack mainHandItem
    ){
        return addCustomMob(
            entityType,
            customName,
            health,
            spawnChance,
            mainHandItem,
            0.1f
        );
    }


    public static boolean addCustomMob(
        String entityType,
        String customName,
        double health,
        int spawnChance,
        ItemStack mainHandItem,
        float dropChance
    ){
        if (health < 0 || health > 1024 || spawnChance < 0) {
            return false;
        }

        return addMobData(entityType, new MobSettings(
            ChatColor.WHITE + customName,
            health,
            spawnChance,
            mainHandItem,
            dropChance
        ));
    }


    public static HashMap<String, MobTypes> getMobsData() {
        return mobsData;
    }


    private static HashMap<String, ArrayList<HashMap<String, ?>>> getRawMobsData()
    {
        HashMap<String, ArrayList<HashMap<String, ?>>> rawMobsData = new HashMap<>();
        mobsData.forEach((entityType, mobTypes) ->
            rawMobsData.put(entityType, mobTypes.getRawTypes())
        );

        return rawMobsData;
    }
}
