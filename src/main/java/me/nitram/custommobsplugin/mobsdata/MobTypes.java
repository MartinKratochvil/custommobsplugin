package me.nitram.custommobsplugin.mobsdata;

import org.bukkit.inventory.ItemStack;
import java.util.ArrayList;
import java.util.HashMap;


public class MobTypes
{

    ArrayList<MobSettings> types;


    public MobTypes(ArrayList<HashMap<String, ?>> rawTypes)
    {
        types = new ArrayList<>();

        rawTypes.forEach(mobRawSettings -> {
            try {
                types.add( new MobSettings(
                    mobRawSettings.get("Name").toString(),
                    (double)mobRawSettings.get("Health"),
                    (int)mobRawSettings.get("SpawnChance"),
                    (ItemStack)mobRawSettings.get("MainHandItem"),
                    (float)(double)mobRawSettings.get("DropChance")
                ));
            }
            catch (Exception ignored) {
                System.out.println("Error in loading custom mob. Data structure in config is invalid, for more details check documentation");
            }
        });
    }


    public MobTypes(MobSettings mobSettings)
    {
        types = new ArrayList<>();
        types.add(mobSettings);
    }


    public ArrayList<MobSettings> getTypes() {
        return types;
    }


    public ArrayList<HashMap<String, ?>> getRawTypes()
    {
        ArrayList<HashMap<String, ?>> rawTypes = new ArrayList<>();
        types.forEach(mobSettings ->
            rawTypes.add(mobSettings.getRawSettings())
        );

        return rawTypes;
    }
}
