package me.nitram.custommobsplugin.listeners;

import me.nitram.custommobsplugin.CustomMobsPlugin;
import me.nitram.custommobsplugin.handlers.PlayerKillLimitHandler;
import me.nitram.custommobsplugin.mobsdata.MobsData;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.Locale;


public class EntityDeathListener implements Listener
{

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        Player playerKiller = event.getEntity().getKiller();
        if (playerKiller == null) {
            return;
        }

        if ( ! MobsData.getMobsData().containsKey(event.getEntityType().name())) {
            return;
        }

        PlayerKillLimitHandler playerKillLimitHandler = CustomMobsPlugin.getInstance().getPlayerLimitHandler();
        playerKillLimitHandler.killCustomMob(playerKiller.getName().toLowerCase());

        if ( ! playerKillLimitHandler.isInLimit(playerKiller.getName().toLowerCase(Locale.ROOT))) {
            playerKiller.sendMessage(ChatColor.RED + "Již jsi přesáhle denní limit zabití!");
            return;
        }
        playerKiller.sendMessage(ChatColor.GREEN + "Uspěšně jsi zabil speciálního moba");

        ItemStack dropItem = new ItemStack(Material.DIAMOND);
        ItemMeta dropItemMeta =  dropItem.getItemMeta();
        if (dropItemMeta == null) {
            return;
        }

        dropItemMeta.setDisplayName(event.getEntity().getName() + " Trophy");
        dropItem.setItemMeta(dropItemMeta);
        event.getDrops().add(dropItem);
    }
}
