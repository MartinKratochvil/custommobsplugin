package me.nitram.custommobsplugin.listeners;

import me.nitram.custommobsplugin.mobsdata.MobSettings;
import me.nitram.custommobsplugin.mobsdata.MobsData;
import me.nitram.custommobsplugin.utils.UtilityChance;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.EntityEquipment;
import java.util.*;


public class MobSpawnListener implements Listener
{

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        String mobName = event.getEntityType().name();
        if ( ! MobsData.getMobsData().containsKey(mobName)) {
            return;
        }

        if ( ! (event.getEntity() instanceof Mob mob)) {
            return;
        }

        ArrayList<MobSettings> mobTypes = MobsData.getMobsData().get(mobName).getTypes();

        if (mobTypes.isEmpty()) {
            return;
        }

        ArrayList<Integer> chances = new ArrayList<>();
        mobTypes.forEach(mobType -> chances.add(mobType.spawnChance()));

        MobSettings mobSettings = mobTypes.get(UtilityChance.Random(chances));

        mob.setCustomName(mobSettings.customName());
        if (mobSettings.health() > 0 && mobSettings.health() <= 1024) {
            mob.setMaxHealth(mobSettings.health());
            mob.setHealth(mobSettings.health());
        }

        EntityEquipment mobEquipment = mob.getEquipment();
        if (mobEquipment == null) {
            return;
        }

        mobEquipment.setItemInMainHand(mobSettings.mainHandItem());
        mobEquipment.setItemInMainHandDropChance(mobSettings.dropChance());
    }
}
