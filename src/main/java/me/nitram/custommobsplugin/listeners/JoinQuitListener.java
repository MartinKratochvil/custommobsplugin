package me.nitram.custommobsplugin.listeners;

import me.nitram.custommobsplugin.CustomMobsPlugin;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;


public class JoinQuitListener implements Listener
{

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        event.setJoinMessage(ChatColor.GREEN + "+ " + ChatColor.GRAY + player.getName());

        FileConfiguration fileConfig = CustomMobsPlugin.getInstance().getMainConfig().get();
        if ( ! fileConfig.contains("Permissions")) {
            return;
        }

        PermissionAttachment attachment = player.addAttachment(CustomMobsPlugin.getInstance());
        for (String permission : fileConfig.getStringList("Permissions")) {
            attachment.setPermission(permission, true);
        }
    }


    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(ChatColor.RED + "- " + ChatColor.GRAY + event.getPlayer().getName());
    }
}
