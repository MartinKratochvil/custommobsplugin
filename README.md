# CustomMobsPlugin

## Getting started

Plugin slouží k vytváření vlastních mobů.

Content:
- [Custom mobs](#custom-mobs)
- [Player kill limit](#player-kill-limit)
- [Mobs Data](#mobs-data)
- [API](#api)
- [Command `/custommobs`](#command-custommobs)

## Custom Mobs

Custom moba můžeme vytvořit z jakéhokoli moba, tato upravená verze se bude spawnovat místo té originální.<br>
Custom mobů můžeme vytvoři kolik chceme, který se spawne místo originálního moba se určuje pomocí spawn chance.

Custom mobu můžeme změnit:
- DisplayName
- Health
- Item in main hand
- Item in main hand drop chance
- Spawn Chance 

Plugin obsahuje již nějaké předvytvořené moby.

## Player kill limit

Každý hráč má limit 10 zabití custom mobů za den.
Po překročení limitu již z mobů nebudou padat trofeje.

Limity zabití se mohou resetovat pomocí příkazu [`/custommobs reload`](#custommobs-reload).

## Mobs Data

Cache ve které jsou uloženy všechny akutální custom mobové je [Mobs Data](src/main/java/me/nitram/custommobsplugin/mobsdata/MobsData.java).

Tyto data jsou linknuté s config filem.<br>
Když upravíme manuálně config, můžeme data načíst do cache pomocí příkazu [`/custommobs reload`](#custommobs-reload).

## API

Custom moby můžeme přidat i z jiného pluginu.
V [Mobs Data](#mobs-data) jsou endpointy pro jejich přidání.

# Command custommobs

Tento command má tři varianty:
- [`/custommobs add`](#custommobs-add)
- [`/custommobs reload`](#custommobs-reload)
- [`/custommobs help`](#custommobs-help)

## Custommobs add

Command `/custommobs add` slouží k přidávání custom mobů.<br>
Na tento command je zapotřeby permission `custommobs.add`.

Command Parameters:<br>
`/custommobs add [MobType] [CustomName] ?[Health 0-1024] ?[SpawnChance >0]`
- MobType | required | string - typ moba např. `creeper`
- CustomName | required | string - jméno moba např. `BOOM`
- Health | unrequired | double | value range <0; 1024> - životy moba např. `20`
- SpawnChance | unrequired | integer | value >0 - šance na spawnutí např. `1` 

Pokud bude Health 0, životy moba budou defaultní.
Čím vyšší SpawnChance, tím vyšší šance na spawnutí tohoto custom moba.

## Custommobs reload

Command `/custommobs reload` slouží k načtení custom mobů z configu a resetování limitu zabití.<br>
Na tento command je zapotřeby permission `custommobs.reload`.

## Custommobs help

Command `/custommobs help` zobrazí help menu.<br>
Na tento command není zapotřeby žádná permission.



